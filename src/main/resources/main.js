var router = require('/lib/router')();
var lovdataApiMock = require('mock/lovdataApi');
var api = require('api');
require('pocRepo').init();

router.post('/legalRecommendationByTitle', api.legalRecommendationByTitle());
router.get('/default', api.default());
router.get('/lovdata', lovdataApiMock.default());
router.post('/lovdata/{lawId}', lovdataApiMock.default());


exports.post = function (req) {
    return router.dispatch(req);
};

exports.get = function (req) {
    return router.dispatch(req);
};






