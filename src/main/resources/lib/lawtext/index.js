var httpClient = require('/lib/http-client');

exports.getLawTextFromApi = function(params){
    var lawId = params.data.lawId;
    var lawParagraph = params.data.lawParagraph;

    var response = httpClient.request({
        url: 'http://localhost:8080/app/poc/lovdata/'+lawId,
        method: 'POST',
        headers: {
            'Cache-Control': 'no-cache'
        },
        connectionTimeout: 5000,
        readTimeout: 5000,
        body: '{"paragraph": "'+lawParagraph+'"}',
        contentType: 'text/xml'
    });

    return response.body;
};