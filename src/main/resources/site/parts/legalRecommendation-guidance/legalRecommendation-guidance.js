var thymeleaf = require('/lib/xp/thymeleaf');
var portalLib = require('/lib/xp/portal');

var view = resolve('legalRecommendation-guidance.html');

exports.get = function (req) {
    var component = portalLib.getComponent();
    var model = {
        guidance: component.config.guidance ? portalLib.processHtml({value:component.config.guidance}) : ''
    };
    return {
        body: thymeleaf.render(view, model)
    }
};