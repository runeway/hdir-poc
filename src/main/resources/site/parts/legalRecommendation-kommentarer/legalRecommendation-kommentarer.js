var thymeleaf = require('/lib/xp/thymeleaf');
var portalLib = require('/lib/xp/portal');
var utilLib = require('/lib/enonic/util');

var view = resolve('legalRecommendation-kommentarer.html');

exports.get = function (req) {
    var component = portalLib.getComponent();
    var model = {
        commentsHeading: component.config.commentsHeading || 'Kommentarer',
        comments:[]
    };

    var comments = utilLib.data.forceArray(component.config.comment);
    comments.forEach(function(comment){
        model.comments.push({
            html: portalLib.processHtml({value:comment})
        });
    });

    return {
        body: thymeleaf.render(view, model)
    }
};