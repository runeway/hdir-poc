var thymeleaf = require('/lib/xp/thymeleaf');
var util = require('/lib/enonic/util');
var contentLib = require('/lib/xp/content');
var portalLib = require('/lib/xp/portal');
var lawTextLib = require('/lib/lawtext');



var contentType = app.name + ':legalRecommendation';
var view = resolve('legalRecommendation-show.html');

exports.get = function (req) {
    var content = portalLib.getContent();

    //Used to render contenttype in preview
    if (content.type === contentType){
        return renderContent({content:content, data:content.data});
    }else{//Used to render contenttype when selected in a part
        var component = portalLib.getComponent();
        if (component.descriptor === contentType){
            return renderContent({content:content, data:component.config});
        }
    }
};


var renderContent = function(params){
    var content = params.content;
    var data = params.data;
    //We need data to render
    if (!data)return;

    var model = params.model || getDefaultModel();
    return renderWithModel({model:getModel({data:data, model:model})});
};

var renderWithModel = function (params){
    var model = params.model || getDefaultModel();
    return {
        body: thymeleaf.render(view, model)
    }
};

var getDefaultModel = function(){return {};};

var getModel = function(params){
    var model = params.model || getDefaultModel();
    var data = params.data;

    if (!data)return;

    model.title = data.title || '';
    model.preface = data.preface ? portalLib.processHtml({value: data.preface}) : '';
    model.guidance = data.guidance ? portalLib.processHtml({value: data.guidance}) : '';

    if (data.lawId && data.lawParagraph){
        model.lawText = lawTextLib.getLawTextFromApi({data:data}) || '';
    }

    return model;

};
