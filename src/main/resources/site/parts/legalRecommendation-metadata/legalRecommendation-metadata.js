var thymeleaf = require('/lib/xp/thymeleaf');
var portalLib = require('/lib/xp/portal');

var view = resolve('legalRecommendation-metadata.html');

exports.get = function (req) {
    var component = portalLib.getComponent();
    var model = {
        category: component.config.category || '',
        department: component.config.department || ''
    };
    return {
        body: thymeleaf.render(view, model)
    }
};