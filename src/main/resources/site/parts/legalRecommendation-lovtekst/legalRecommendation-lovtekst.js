var thymeleaf = require('/lib/xp/thymeleaf');
var portalLib = require('/lib/xp/portal');
var lawTextLib = require('/lib/lawtext');

var view = resolve('legalRecommendation-lovtekst.html');

exports.get = function (req) {
    var component = portalLib.getComponent();
    var model = {
        sectionHeading: component.config.sectionHeading || 'Hjemmel',
        lawHeading: component.config.lawTextHeading || 'Lovtekst'
    };

    if (component.config.lawId && component.config.lawParagraph){
        model.lawText = lawTextLib.getLawTextFromApi({data:component.config}) || '';
    }

    return {
        body: thymeleaf.render(view, model)
    }
};