var thymeleaf = require('/lib/xp/thymeleaf');
var portalLib = require('/lib/xp/portal');

var view = resolve('legalRecommendation-title.html');

exports.get = function (req) {
    var component = portalLib.getComponent();
    var model = {
        title: component.config.title || ''
    };
    return {
        body: thymeleaf.render(view, model)
    }
};