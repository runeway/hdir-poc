var contentLib = require('/lib/xp/content');

exports.legalRecommendationByTitle = function () {
    return function (req) {
        if (!req.params.title)return;
        var result = contentLib.query({
            filters: {
                boolean: {
                    must: {
                        hasValue: {
                            field: 'data.title',
                            values: [req.params.title]
                        }
                    }
                }
            }
        });
        var data = result.hits[0].data;
        if (req.params.stripHtml){
            data.guidance = data.guidance.replace(/(<([^>]+)>)/ig,"");
            data.preface = data.preface.replace(/(<([^>]+)>)/ig,"");

        }
        return {
            body:data,
            contentType:'application/json'
        }

    }
};

exports.default = function(){
    return function(req){
        return {
            body:{
                name: "lawCommentary",
                title: "Lovtekst",
                type: "document",
                fields: [
                    {
                        name: 'title',
                        title: 'Tittel',
                        type: 'string'
                    },
                    {
                        name: 'text',
                        title: 'Tekst',
                        type: 'blockText',
                    },
                    {
                        name: 'recommendations',
                        title: 'Lovkrav',
                        type: 'array',
                        of: [
                            {
                                type: 'object',
                                fields: [
                                    {
                                        name: 'title',
                                        title: 'Tittel',
                                        type: 'string'
                                    },
                                    {
                                        name: 'text',
                                        title: 'Ingress',
                                        type: 'blockText',
                                    },
                                    {
                                        name: 'intent',
                                        type: 'string',
                                        title: 'Intensjons-id',
                                        description: 'Knytt innhold til Dialogflow'
                                    },
                                    {
                                        name: 'fullfillments',
                                        title: 'Greier som en dialogdingser sier',
                                        type: 'array',
                                        of: [
                                            {
                                                type: 'string',
                                                title: 'Svar-variasjon'
                                            }
                                        ]
                                    },
                                    {
                                        name: 'practicalAdvice',
                                        title: 'Veiledning',
                                        type: 'blockText',
                                    },
                                    {
                                        name: 'rationale',
                                        title: 'Hjemmel',
                                        type: 'object',
                                        fields: [
                                            {
                                                name: 'text',
                                                title: 'Lovtekst',
                                                type: 'blockText'
                                            },
                                            {
                                                name: 'comments',
                                                title: 'Kommentarer',
                                                type: 'blockText'
                                            }
                                        ]
                                    }
                                ]}
                        ],
                        options: {
                            editModal: 'fullscreen'
                        }
                    },
                ]
            },
            contentType:'application/json'

        }
    }
}