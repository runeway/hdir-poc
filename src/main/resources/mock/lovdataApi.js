var xsltLib = require('/lib/xslt');
var xsltView = resolve('lovdata.xslt');
var ioLib = require('/lib/xp/io');
var xml = require('lovdataXMLFile');
var paragraph = require('lovdataText');

var encodingLib = require('/lib/text-encoding');


exports.default = function () {
    return function (req) {
        if (req.pathParams.lawId) {
            log.info('Hent lovtekst med id %s', req.pathParams.lawId);
        }
        var body = {};
        if (req.body){
            body = JSON.parse(req.body);
        }
        if (body.paragraph){//return only paragraph
            /*var xmlModel = {
                paragraph: body.paragraph
            };
            var xsltResult = xsltLib.render(xsltView, xmlModel);
            log.info("%s",xsltResult);*/
            return {
                body: paragraph.txt,
                contenttype: 'text/html'
            }

        }else{//return entire xml
            return {
                body: xml,
                contentType:'text/xml'
            }
        }
    }
};