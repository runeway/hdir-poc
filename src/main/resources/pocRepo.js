var appersist = require('/lib/openxp/appersist');
var repoConnection = appersist.repository.getConnection();

exports.init = function(){
    var temaer = ['ADHD', 'Akuttmedisin', 'Alkohol', 'Antibiotika', 'Autorisasjon og lisens', 'Beredskap og krisehåndtering',
        'CFS-ME', 'Demens', 'Diabetes', 'Finansieringsordninger', 'Frisklivssentraler', 'Fristbrudd', 'Fritt behandlingsvalg',
        'Fysisk aktivitet', 'Individuell plan, rehabilitering og habilitering', 'Graviditet, fødsel og barsel', 'Helsepersonellregisteret (HPR)',
        'Helsestasjons- og skolehelsetjenesten', 'Innvandrerhelse', 'IPLOS-registeret', 'ISF og DRG',
        'Kommunalt pasient- og brukerregister (KPR)', 'Kosthold og ernæring', 'Kreft', 'Legemidler', 'Motiverende intervju',
        'Norsk pasientregister (NPR)', 'Nødnett', 'Prioritering', 'Psykisk helse og rus', 'blaPsykisk utviklingshemmingm',
        'Pårørende', 'Seksuell helse', 'Spesialistgodkjenning', 'Statistikk og analyse', 'Sykehjem og hjemmetjenester',
        'Sykmelding', 'Tauhetsplikt', 'Velferdsteknologi', 'Venteliste og ventetider' ];

    var avdelinger = ['Miljø og helse', 'Barne- og ungdomshelse', 'Psykisk helse og rus', 'Folkesykdommer', 'Levekår',
        'Retningslinjer og fagutvikling', 'Velferdsteknologi og rehabilitering', 'Kvalitetsforbedring og pasientsikkerhet',
        'Legevakt og akuttmedisin', 'Kommunale helse- og omsorgstjenester', 'Spesialisthelsetjenester', 'Finansiering',
        'Helserefusjoner','Tilskudd','Autorisasjon','Personell og godkjenning','Global helse og dokumentasjon',
        'Komparativ statistikk og styringsinformasjon','Beredskap','Helserett og bioteknologi','Helseregistre',
        'Kommunikasjon','Utvikling og digitale kanaler'];

    try {
        repoConnection.create({_name: 'avdeling'});
    }catch(e){
        log.info('rotnoden avdeling eksisterer fra før');
    }
    try {
        repoConnection.create({_name: 'tema'});
    }catch(e){
        log.info('rotnoden tema eksisterer fra før');
    }

    temaer.forEach(function(tema){
        try{
            repoConnection.create({_parentPath: '/tema', _name:tema});
        }catch (e){
            log.info('Tema %s eksisterer fra før', tema);
        }
    });

    avdelinger.forEach(function(avdeling){
        try{
            repoConnection.create({_parentPath: '/avdeling', _name:avdeling});
        }catch (e){
            log.info('Avdeling %s eksisterer fra før', avdeling);
        }
    });
};

