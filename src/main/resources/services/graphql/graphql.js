var guillotineLib = require('/lib/guillotine');
var graphQlLib = require('/lib/graphql');
var contentLib = require('/lib/xp/content');

var schema = guillotineLib.createSchema();

// POST request handler that will execute the query received as parameter against the schema defined above.
exports.post = function (req) {
    var body = JSON.parse(req.body);
    var result = graphQlLib.execute(schema, body.query, body.variables);
    return {
        contentType: 'application/json',
        body: result
    };
};

var legalRecommendationType = graphQlLib.createObjectType({
    name: 'LegalRecommendation',
    description: 'A legalRecommendation type.',
    fields: {
        title: {
            type: graphQlLib.nonNull(graphQlLib.GraphQLString),
            resolve: function (env) {
                return env.source.title;
            }
        },
        guidance: {
            type: graphQlLib.nonNull(graphQlLib.GraphQLString),
            resolve: function (env) {
                return env.source.guidance;
            }
        },
        category: {
            type: graphQlLib.nonNull(graphQlLib.GraphQLString),
            resolve: function (env) {
                return env.source.category;
            }
        },
        department: {
            type: graphQlLib.nonNull(graphQlLib.GraphQLString),
            resolve: function (env) {
                return env.source.department;
            }
        }
    }
});

// Defines a second object type 'Query', the root object type containing all the root retrieval requests.
var rootQueryType = graphQlLib.createObjectType({
    name: 'Query',
    fields: {
        getLegalRecommendationsByTitle: {
            type: legalRecommendationType,
            args: {
                title: graphQlLib.nonNull(graphQlLib.GraphQLString),
                stripHtml: graphQlLib.GraphQLBoolean
            },
            resolve: function (env) {
                var title = env.args.title;
                var result = contentLib.query({
                    filters: {
                        boolean: {
                            must: {
                                hasValue: {
                                    field: 'data.title',
                                    values: [title]
                                }
                            }
                        }
                    }
                });
                if (result && result.hits && result.hits[0]) {
                    if (env.args.stripHtml) {
                        var data = result.hits[0].data;
                        /*Simple strip html example*/
                        data.guidance = data.guidance.replace(/(<([^>]+)>)/ig,"");
                        data.preface = data.preface.replace(/(<([^>]+)>)/ig,"");
                        return data;
                    } else {
                        return result.hits[0].data;
                    }

                }
            }
        }
    }
});

// Define the GraphQL schema
var schema = graphQlLib.createSchema({
    query: rootQueryType
});