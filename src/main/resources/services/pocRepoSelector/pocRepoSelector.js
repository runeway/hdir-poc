var portalLib = require('/lib/xp/portal');
var appersist = require('/lib/openxp/appersist');
var repoConnection = appersist.repository.getConnection();

exports.get = handleGet;

function handleGet(req) {

    var params = parseparams(req.params);
    var body = createresults(getItems(req.params), params);
    return {
        contentType: 'application/json',
        body: body
    }
}

function parseparams(params) {

    var query = params['query'],
        ids, start, count;

    try {
        ids = JSON.parse(params['ids']) || []
    } catch (e) {
        log.warning('Invalid parameter ids: %s, using []', params['ids']);
        ids = [];
    }

    try {
        start = Math.max(parseInt(params['start']) || 0, 0);
    } catch (e) {
        log.warning('Invalid parameter start: %s, using 0', params['start']);
        start = 0;
    }

    try {
        count = Math.max(parseInt(params['count']) || 15, 0);
    } catch (e) {
        log.warning('Invalid parameter count: %s, using 15', params['count']);
        count = 15;
    }

    return {
        query: query,
        ids: ids,
        start: start,
        end: start + count,
        count: count
    }
}

function createresults(items, params, total) {

    var body = {};

    var hitCount = 0, include;
    body.hits = items.sort(function (hit1, hit2) {
        if (!hit1 || !hit2) {
            return !!hit1 ? 1 : -1;
        }
        return hit1.displayName.localeCompare(hit2.displayName);
    }).filter(function (hit) {
        include = true;

        if (!!params.ids && params.ids.length > 0) {
            include = params.ids.some(function (id) {
                return id == hit.id;
            });
        } else if (!!params.query && params.query.trim().length > 0) {
            var qRegex = new RegExp(params.query, 'i');
            include = qRegex.test(hit.displayName) || qRegex.test(hit.description) || qRegex.test(hit.id);
        }

        if (include) {
            hitCount++;
        }
        return include && hitCount > params.start && hitCount <= params.end;
    });
    body.count = Math.min(params.count, body.hits.length);
    body.total = params.query ? hitCount : (total || items.length);

    return body;
}


/*
* {
    id: "id",
    displayName: "displayName",
    description: "description"
}*/
function getItems(params) {
    var items = [];

    if (!params.path) return items;

    var idsResult = repoConnection.query({start:0, count:99, query:"_parentPath LIKE '" + params.path + "'"});

    if (!idsResult || !idsResult.hits) return items;

    var ids = [];
    idsResult.hits.forEach(function (hit){
        ids.push(hit.id);
    });

    var results = repoConnection.get(ids);

    results.forEach(function(result){
        items.push({id:result._name, displayName: result._name, description: result._name});
    });


    return items;
}
