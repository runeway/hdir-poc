##Lovdata mock api 
Xml fra filen nl-19990702-064.xml er tilgjengelig på 
http://localhost:8080/app/poc/lovdata (GET)
http://localhost:8080/app/poc/lovdata/lov-1999-07-02-64 (POST)

##Metadata

###Avdelinger og Tema
Disse er lagt i et cusom repo for å slippe at kode må oppdateres for å legge til nye 
tema eller avdelinger, men en superbruker kan administrere det selv. 
De kan hentes ut via f.eks. en CustomSelector i kode. Se eksempel på bruk i innholdstypen 
legalRecommendation.xml

For å se data, bruk app Data Toolbox og bla deg frem til path 
Data Toolbox / Data Tree / poc / master / root / [tema | avdelinger]


##Graphql
Install graphiql app open it 
(http://localhost:8080/admin/tool/com.enonic.app.graphiql/graphiql)

Example query:

{
  getLegalRecommendationsByTitle(
    title: "Samtykke fra pasient",
  	stripHtml:true) {
    title
    guidance
  }
}


##Rest API

POST http://localhost:8080/app/poc/legalRecommendationByTitle?title=Samtykke fra pasient&stripHtml=true
POST http://localhost:8080/app/poc/legalRecommendationByTitle?title=Samtykke fra pasient&stripHtml=true

GET http://localhost:8080/app/poc/default (returnerer ønsket dummy json)